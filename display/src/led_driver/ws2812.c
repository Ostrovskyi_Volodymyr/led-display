#include "ws2812.h"

#define CHANNEL_OFF 0x88888888
#define BIT_ON 0xC

static void _set_led(struct RGB_LED *led, uint8_t r, uint8_t g, uint8_t b);
static void _set_channel(uint32_t *c, uint8_t v);
static void _configure(void *handler);
static void _start_sending(uint32_t *buf, uint16_t size);
static void _stop_sending(void);

struct _LED_DRIVER WS2812 = {
	.set_led = _set_led,
	.configure = _configure,
	.start_sending = _start_sending,
	.stop_sending = _stop_sending
};

static void _set_led(struct RGB_LED *led, uint8_t r, uint8_t g, uint8_t b) {
	// TODO: несколько возможных интерфейсов для передачи
	led->r = CHANNEL_OFF;
	led->g = CHANNEL_OFF;
	led->b = CHANNEL_OFF;
	if (r != 0)
		_set_channel(&led->r, r);
	if (g != 0)
		_set_channel(&led->g, g);
	if (b != 0)
		_set_channel(&led->b, b);
}

static void _set_channel(uint32_t *c, uint8_t v) {
	uint32_t val = *c;
	for (uint8_t i = 0; i < 8; ++i) {
		if ((1 << i) & v) {
			uint32_t mask = ~(0x0f << 4 * i);
			uint32_t on = (BIT_ON << 4 * i);
			val = (val & mask) | on;
		}
	}
	val = (val >> 16) | (val << 16);
	*c = val;
}

static void _configure(void *handler) {
	nrfx_i2s_config_t conf = NRFX_I2S_DEFAULT_CONFIG;
	conf.sdin_pin = NRFX_I2S_CONFIG_SDIN_PIN;
	conf.sdout_pin = NRFX_I2S_CONFIG_SDOUT_PIN;
	conf.mck_setup = NRF_I2S_MCK_32MDIV10;
	conf.ratio = NRF_I2S_RATIO_32X;
	conf.channels = NRF_I2S_CHANNELS_STEREO;
	conf.sample_width = NRF_I2S_SWIDTH_16BIT;
	conf.mode = NRF_I2S_MODE_MASTER;
	nrfx_i2s_init(&conf, (nrfx_i2s_data_handler_t)handler);
}

static void _start_sending(uint32_t *buf, uint16_t size) {
	nrfx_i2s_buffers_t i2s_bufs = {.p_rx_buffer = NULL, .p_tx_buffer = buf};
	nrfx_i2s_start(&i2s_bufs, size, 0);
}

static void _stop_sending(void) {
	nrfx_i2s_stop();
}