#include <stdint.h>

struct RGB_LED {
	uint32_t g, r, b;
};

struct _LED_DRIVER {
	void(*const set_led)(struct RGB_LED*, uint8_t r, uint8_t g, uint8_t b);
	void(*const configure)(void *handler);
	void(*const start_sending)(uint32_t *buf, uint16_t size);
	void(*const stop_sending)(void);
};