#include <stdbool.h>
#include <stdint.h>

#include "nrf.h"
#include "nrfx.h"
#include "nrf_delay.h"
#include "led_driver/ws2812.h"
#include "controll_over_ble/coble.h"

#define NLEDS 10
#define RESET 8
#define I2S_BUFFER_SIZE NLEDS * 3 + RESET * 3

struct RGB_LED colors[] = {
	{.r = 255, .g = 0, .b = 0},
	{.r = 255, .g = 128, .b = 0},
	{.r = 255, .g = 255, .b = 0},
	{.r = 0, .g = 255, .b = 0},
	{.r = 0, .g = 255, .b = 255},
	{.r = 0, .g = 0, .b = 255},
	{.r = 128, .g = 0, .b = 255}
};

static int color_pos = 0;

struct RGB_LED leds[NLEDS + RESET];

void i2s_data_handler(nrfx_i2s_buffers_t const *p_released, uint32_t status) {}

typedef enum {STATE_CONNECT, STATE_DISCONNECT, STATE_RAINBOW, STATE_BLUE} led_state_t;
led_state_t st = STATE_DISCONNECT;

void on_write(const uint8_t *data) {
	if (data[0] == 10)
		st = STATE_RAINBOW;
	else if (data[0] == 99) {
		st = STATE_BLUE;
	}
}

void on_connect(void) {
	st = STATE_CONNECT;
}

void on_disconnect(void) {
	st = STATE_DISCONNECT;
}

int main(void)
{
	WS2812.configure(i2s_data_handler);
	WS2812.start_sending((uint32_t*)leds, I2S_BUFFER_SIZE);
	coble_init_t coble_init_s;
	coble_init_s.on_write = on_write;
	coble_init_s.on_connect = on_connect;
	coble_init_s.on_disconnect = on_disconnect;
	coble_init(&coble_init_s);
	//WS2812.stop_sending();
	//int led = 0;
	for (;;) {
		switch (st) {
			case STATE_DISCONNECT:
				{
					for (uint32_t i = 0; i < NLEDS; ++i) {
						WS2812.set_led(leds + i, 200, 0, 0);
					}
					break;
				}
			case STATE_CONNECT:
				{
					for (uint32_t i = 0; i < NLEDS; ++i) {
						WS2812.set_led(leds + i, 0, 200, 0);
					}
					break;
				}
			case STATE_RAINBOW:
				{
					for (uint32_t i = 0; i < NLEDS; ++i) {
						struct RGB_LED cl = colors[(color_pos + i) % 7];
						WS2812.set_led(leds + i, cl.r, cl.g, cl.b);
					}
					color_pos = (color_pos + 1) % 7;
					break;
				}
			case STATE_BLUE:
				{
					for (uint32_t i = 0; i < NLEDS; ++i) {
						WS2812.set_led(leds + i, 0, 0, 200);
					}
					break;
				}
		}
		//led = (led + 1) % NLEDS;
		nrf_delay_ms(400);
	}
}
