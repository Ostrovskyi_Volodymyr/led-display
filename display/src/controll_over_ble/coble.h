#ifndef COBLE_H__
#define COBLE_H__

#include "nordic_common.h"
#include "nrf.h"
#include "app_timer.h"
#include "app_error.h"
#include "ble.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "fds.h"
#include "peer_manager.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

typedef void (*__write_event)(const uint8_t*);
typedef void (*__connect_event)(void);
typedef void (*__disconnect_event)(void);

typedef struct {
	__write_event on_write;
	__connect_event on_connect;
	__disconnect_event on_disconnect;
} coble_init_t;

void coble_init(coble_init_t *p_coble_init);

#endif // COBLE_H__