#ifndef CTRL_SERVICE_H__
#define CTRL_SERVICE_H__

#include <stdint.h>
#include <string.h>

#include "ble.h"
#include "ble_srv_common.h"
#include "app_error.h"

#define BLE_UUID_LIB_BASE_UUID              {0xBC, 0x8A, 0xBF, 0x45, 0xCA, 0x05, 0x50, 0xBA, 0x40, 0x42, 0xB0, 0x00, 0xC9, 0xAD, 0x64, 0xF3}
#define BLE_UUID_CTRL_SERVICE                0x818A
#define BLE_UUID_KEYCODE_CHAR_UUID     		 0xBEEF

typedef void (*__write_event)(const uint8_t*);
typedef void (*__connect_event)(void);
typedef void (*__disconnect_event)(void);

typedef struct {
    uint16_t    service_handle;
    uint16_t    conn_handle;
    ble_gatts_char_handles_t keycode_char;
    uint8_t     uuid_type;
    __write_event on_write;
	__connect_event on_connect;
	__disconnect_event on_disconnect;
} ble_ctrl_service_t;

typedef struct {
	__write_event on_write;
	__connect_event on_connect;
	__disconnect_event on_disconnect;
} ble_ctrl_service_init_t;

void ctrl_service_init(ble_ctrl_service_t *p_ctrl_service, ble_ctrl_service_init_t *p_ctrl_service_init);
void ble_ctrl_service_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

#endif // CTRL_SERVICE_H__