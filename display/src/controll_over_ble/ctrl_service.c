#include "ctrl_service.h"

void ble_ctrl_service_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context) {
  	ble_ctrl_service_t * p_ctrl_service =(ble_ctrl_service_t*)p_context;  

        switch (p_ble_evt->header.evt_id) {
            case BLE_GAP_EVT_CONNECTED:
                p_ctrl_service->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
				p_ctrl_service->on_connect();
                break;
            case BLE_GAP_EVT_DISCONNECTED:
                p_ctrl_service->conn_handle = BLE_CONN_HANDLE_INVALID;
				p_ctrl_service->on_disconnect();
                break;
            case BLE_GATTS_EVT_WRITE: {
                       ble_gatts_evt_write_t const* p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
                        if (p_evt_write->handle == p_ctrl_service->keycode_char.value_handle) {
                            p_ctrl_service->on_write(p_evt_write->data);
                        }
                        break;
            }
        }
}

static uint32_t keycode_char_add(ble_ctrl_service_t *p_ctrl_service) {
    uint32_t err_code;
    ble_uuid_t char_uuid;
    ble_uuid128_t base_uuid = {BLE_UUID_LIB_BASE_UUID};
    char_uuid.uuid = BLE_UUID_KEYCODE_CHAR_UUID;
    err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
    APP_ERROR_CHECK(err_code);

    ble_gatts_char_md_t char_md;
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read = 1;
    char_md.char_props.write = 1;
    
    ble_gatts_attr_md_t attr_md;
    memset(&attr_md, 0, sizeof(attr_md));  
    attr_md.vloc = BLE_GATTS_VLOC_STACK;
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	
    ble_gatts_attr_t    attr_char_value;
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid = &char_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.max_len = 2;
    uint8_t value[] = {0x0, 0x0};
    attr_char_value.p_value = value;
    attr_char_value.init_len = 2;

    err_code = sd_ble_gatts_characteristic_add(p_ctrl_service->service_handle, 
                                                &char_md, &attr_char_value, 
                                                &p_ctrl_service->keycode_char);
    APP_ERROR_CHECK(err_code);
    return NRF_SUCCESS;
}

void ctrl_service_init(ble_ctrl_service_t *p_ctrl_service, ble_ctrl_service_init_t *p_ctrl_service_init) {
	uint32_t err_code;
	ble_uuid_t service_uuid;
	ble_uuid128_t base_uuid = {BLE_UUID_LIB_BASE_UUID};
	
	service_uuid.uuid = BLE_UUID_CTRL_SERVICE;
	err_code = sd_ble_uuid_vs_add(&base_uuid, &p_ctrl_service->uuid_type);
	APP_ERROR_CHECK(err_code);

	service_uuid.type = p_ctrl_service->uuid_type;
	p_ctrl_service->conn_handle = BLE_CONN_HANDLE_INVALID;
	p_ctrl_service->on_write = p_ctrl_service_init->on_write;
	p_ctrl_service->on_connect = p_ctrl_service_init->on_connect;
	p_ctrl_service->on_disconnect = p_ctrl_service_init->on_disconnect;
	
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, 
      &service_uuid,                                              
      &p_ctrl_service->service_handle);
    APP_ERROR_CHECK(err_code);
	keycode_char_add(p_ctrl_service);
}